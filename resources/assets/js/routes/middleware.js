import Auth from '../services/Auth';

function auth(to, from, next) {
  const jwt = Auth.getJwt();

  if (jwt === null || !jwt) {
    next({ name: 'index' });
  } else {
    Auth.refetchToken();
    next();
  }
}

export default {
  auth,
};
