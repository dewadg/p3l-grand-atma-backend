<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class RoomType extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'size',
        'price',
        'capacity',
        'image_id',
    ];

    /**
     * Hidden attributes.
     *
     * @var array
     */
    protected $hidden = [
        'image_id',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'price' => 'double',
    ];

    /**
     * Set primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship to Facility.
     *
     * @return array
     */
    public function facilities()
    {
        return $this->belongsToMany(Facility::class, 'facility_room_type_map');
    }

    /**
     * Relationship to Season.
     *
     * @return array
     */
    public function seasons()
    {
        return $this->belongsToMany(Season::class, 'room_type_season_map', 'room_type_id')
            ->withPivot(['price']);
    }

    /**
     * Returns current Season.
     *
     * @return Season
     */
    public function currentSeason()
    {
        $today = Carbon::now();

        return $this->seasons
            ->filter(function ($item) use ($today) {
                return $today->gte($item->start)
                    && $today->lte($item->end);
            })
            ->first();
    }

    /**
     * Returns current price of the RoomType
     * according to Season.
     *
     * @return double
     */
    public function currentPrice()
    {
        return is_null($this->currentSeason())
            ? $this->price
            : $this->currentSeason()->pivot->price;
    }

    /**
     * Relationship to Room.
     *
     * @return array
     */
    public function rooms()
    {
        return $this->hasMany(Room::class, 'room_type_id');
    }

    /**
     * Relationship to Image.
     *
     * @return Image
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
