<?php

use Illuminate\Database\Seeder;
use App\PaymentStatus;

class PaymentStatusesTableSeeder extends Seeder
{
    private $data = [
        ['id' => 1, 'name' => 'Menunggu Pembayaran'],
        ['id' => 2, 'name' => 'Dibayar'],
        ['id' => 3, 'name' => 'Dibatalkan'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $payment_status) {
            PaymentStatus::create($payment_status);
        }
    }
}
