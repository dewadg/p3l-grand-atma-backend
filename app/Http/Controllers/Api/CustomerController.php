<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Returns available customers.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::with(['user', 'creditCards'])->get();
    }

    /**
     * Stores new customer.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'phone' => 'required|max:12',
                'address' => 'required',
                'id_number' => 'required',
                'type' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                Customer::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns single customer.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $customer = Customer::with('user')->find($id);

        if (is_null($customer)) {
            return response()->json(null, 404);
        }

        return $customer;
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'phone' => 'required|max:12',
                'address' => 'required',
                'id_number' => 'required',
                'type' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($customer, $user_data) {
                $customer->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a customer.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($customer) {
                $customer->delete();
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
