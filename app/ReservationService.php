<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ReservationService extends Pivot
{
    /**
     * Sets the table.
     *
     * @var string
     */
    protected $table = 'reservation_service_map';

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'double',
        'subtotal' => 'double',
    ];
}
