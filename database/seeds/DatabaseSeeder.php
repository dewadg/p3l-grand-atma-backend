<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BranchesTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(BedTypesTableSeeder::class);
        $this->call(ReservationStatusesTableSeeder::class);
        $this->call(PaymentStatusesTableSeeder::class);
        $this->call(PaymentTypesTableSeeder::class);
    }
}
