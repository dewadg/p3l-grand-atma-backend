<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/panel', 'IndexController@panel');
Route::get('report/customers/{year}', 'ReportController@customers');
Route::get('report/room-types/{year}/{month}', 'ReportController@roomTypes');
Route::get('report/top-customers/{year}', 'ReportController@topCustomers');
Route::get('report/monthly-incomes/{year}', 'ReportController@monthlyIncomes');
Route::get('report/yearly-incomes-per-branches/{year}', 'ReportController@yearlyIncomesPerBranches');
