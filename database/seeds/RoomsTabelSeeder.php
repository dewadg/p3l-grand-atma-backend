<?php

use Illuminate\Database\Seeder;
use App\Room;

class RoomsTabelSeeder extends Seeder
{
    private $data = [
        ['id' => 'JOG0101SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => true],
        ['id' => 'JOG0102SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0103SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => true],
        ['id' => 'JOG0104SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0105SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0106SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0107SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0108SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],
        ['id' => 'JOG0109SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],
        ['id' => 'JOG0110SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],
        ['id' => 'JOG0111SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],
        ['id' => 'JOG0112SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],
        ['id' => 'JOG0115SP', 'room_type_id' => 'SP', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],

        ['id' => 'JOG0101DD', 'room_type_id' => 'DD', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0102DD', 'room_type_id' => 'DD', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => true],
        ['id' => 'JOG0103DD', 'room_type_id' => 'DD', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => true],
        ['id' => 'JOG0104DD', 'room_type_id' => 'DD', 'branch_id' => 'JOG', 'bed_type_id' => 2, 'smoking' => false],
        ['id' => 'JOG0105DD', 'room_type_id' => 'DD', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],



        ['id' => 'JOG0101ED', 'room_type_id' => 'ED', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0102ED', 'room_type_id' => 'ED', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => true],
        ['id' => 'JOG0103ED', 'room_type_id' => 'ED', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0104ED', 'room_type_id' => 'ED', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => true],
        ['id' => 'JOG0105ED', 'room_type_id' => 'ED', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],

        ['id' => 'JOG0101JS', 'room_type_id' => 'JS', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => true],
        ['id' => 'JOG0102JS', 'room_type_id' => 'JS', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0103JS', 'room_type_id' => 'JS', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0104JS', 'room_type_id' => 'JS', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
        ['id' => 'JOG0105JS', 'room_type_id' => 'JS', 'branch_id' => 'JOG', 'bed_type_id' => 1, 'smoking' => false],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $room) {
            Room::create($room);
        }
    }
}
