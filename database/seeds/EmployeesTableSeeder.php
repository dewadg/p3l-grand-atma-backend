<?php

use Illuminate\Database\Seeder;
use App\Employee;

class EmployeesTableSeeder extends Seeder
{
    private $data = [
        ['id' => 1, 'user_id' => 1, 'branch_id' => 'JOG'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $employee) {
            Employee::create($employee);
        }
    }
}
