<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Image;

class ImageController extends Controller
{
    /**
     * Uploads an image.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            ['file' => 'required|file']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $image = null;

            DB::transaction(function () use ($request, &$image) {
                $file_name = $request->file('file')->getClientOriginalName();
                $path = $request->file('file')->storeAs('images', $file_name, 'public');

                $image = Image::create([
                    'file_name' => $file_name,
                    'path' => $path,
                ]);

                $image->url = $image->url();
            });

            return response()->json($image, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes an image.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Image::find($id);

        if (is_null($image)) {
            return response()->json('image_not_found', 404);
        }

        Storage::disk('public')->delete($image->path);

        return response()->json();
    }
}
