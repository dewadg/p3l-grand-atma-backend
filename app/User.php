<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Relationship to Role.
     *
     * @return Role
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Relationship to Employee.
     *
     * @return Employee
     */
    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    public function authTokens()
    {
        return $this->hasMany(AuthToken::class);
    }

    /**
     * Generates JWT
     *
     * @return string
     */
    public function getJwt()
    {
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuer(env('APP_URL'))
            ->setAudience((env('APP_URL')))
            ->setIssuedAt(time())
            ->set('user', $this)
            ->sign($signer, env('JWT_SIGN_KEY'))
            ->getToken();

        $this->authTokens()->create([
            'token' => (string) $token,
        ]);

        return (string) $token;
    }
}
