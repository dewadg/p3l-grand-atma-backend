<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use App\Services\RoomService;

class RoomServiceTest extends TestCase
{
    /**
     * Stores the service instance.
     *
     * @var RoomService
     */
    protected $service;

    public function __construct()
    {
        parent::__construct();

        $this->service = new RoomService;
    }

    public function testAvailableRooms()
    {
        $start = Carbon::createFromFormat('Y-m-d', '2018-04-24');
        $end = $start->copy()->addDays(3);
        $available_rooms = $this->service->availableRooms($start, $end)->all();

        $this->assertTrue(is_array($available_rooms));
    }
}
