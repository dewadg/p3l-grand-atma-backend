import {
  required,
  maxLength,
} from 'vuelidate/lib/validators';

export default {
  name: { required },
  phone: {
    required,
    maxLength: maxLength(12),
  },
  address: { required },
};
