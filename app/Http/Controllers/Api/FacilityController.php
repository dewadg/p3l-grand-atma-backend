<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Facility;

class FacilityController extends Controller
{
    /**
     * Returns available facilities.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Facility::get();
    }

    /**
     * Stores new facility.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), ['name' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                Facility::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns single facility.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $facility = Facility::find($id);

        if (is_null($facility)) {
            return response()->json(null, 404);
        }

        return $facility;
    }

    /**
     * Updates a facility.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $facility = Facility::find($id);

        if (is_null($facility)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make($request->all(), ['name' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('id');

            DB::transaction(function () use ($facility, $user_data) {
                $facility->update($user_data);
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a facility
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility = Facility::find($id);

        if (is_null($facility)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($facility) {
                $facility->delete();
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
