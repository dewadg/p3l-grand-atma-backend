<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ReservationStatus;

class ReservationStatusController extends Controller
{
    /**
     * Returns available reservation statuses.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return ReservationStatus::orderBy('name', 'ASC')->get();
    }
}
