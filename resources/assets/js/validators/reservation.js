import { required, minLength } from 'vuelidate/lib/validators';

export default {
  form: {
    customer_id: { required },
    reservation_status_id: { required },
    num_of_adults: { required },
    start: { required },
    end: { required },
    payment_type_id: { required },
    payment_status_id: { required },
  },
  selectedRooms: {
    required,
    minLength: minLength(1),
  },
};
