@extends('pdf.master')
@section('style')
  .year {
    display: block;
    margin-bottom: 15px;
  }

  .report {
    width: 100%;
    border-collapse: collapse;
  }

  .report thead tr th,
  .report tbody tr td,
  .report tfoot tr td {
    padding: 5px;
  }

  .report tfoot tr td {
    font-weight: bold;
  }
@endsection
@section('content')
  <h2 class="text-center">LAPORAN 5 CUSTOMER RESERVASI TERBANYAK</h2>
  <span class="year">Tahun: {{ $year }}</span>
  <table class="report" border="1">
    <thead>
      <tr>
        <th class="text-center" width="50">No.</th>
        <th class="text-center">Nama Customer</th>
        <th class="text-center">Jumlah Reservasi</th>
        <th class="text-center">Total Pembayaran</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $row)
        <tr>
          <td class="text-center">{{ ($loop->index + 1) }}</td>
          <td>{{ $row['name'] }}</td>
          <td class="text-center">{{ $row['reservations'] }}</td>
          <td class="text-right">Rp {{ number_format($row['total'], 0, ',', '.') }}</td>
        </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td class="text-right" colspan="3">TOTAL</td>
        <td class="text-right">Rp {{ number_format($total, 0, ',', '.') }}</td>
      </tr>
    </tfoot>
  </table>
  <p class="text-right">Dicetak pada {{ date('j F Y H:i') }}</p>
@endsection
