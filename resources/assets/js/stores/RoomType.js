import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get('room-types', successCallback, errorCallback);
    });
  },

  fetchForReservation(context, payload) {
    return new Promise((resolve, reject) => {
      const {
        start,
        end,
      } = payload;

      const url = `reservations/available-rooms/${start}/${end}`;

      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get(url, successCallback, errorCallback);
    });
  },

  store(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        if (res.status === 201) {
          resolve();
        }
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.post('room-types', payload, successCallback, errorCallback);
    });
  },

  fetch(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get(`room-types/${id}`, successCallback, errorCallback);
    });
  },

  update(context, payload) {
    return new Promise((resolve, reject) => {
      const { id } = payload;

      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.patch(`room-types/${id}`, payload, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.delete(`room-types/${id}`, successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
