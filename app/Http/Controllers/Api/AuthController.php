<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
    /**
     * Authenticates a user.
     *
     * @param Request $request
     * @return Illuminate\Http\Request
     */
    public function authenticate(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'email' => 'required|email',
                'password' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        if (Auth::attempt($request->only('email', 'password'))) {
            if (! is_null(Auth::user()->employee)) {
                $jwt = User::with([
                        'role' => function ($query) {
                            $query->select(['id', 'name']);
                        },
                        'employee' => function ($query) {
                            $query->select(['id', 'user_id', 'branch_id']);
                        },
                        'employee.branch' => function ($query) {
                            $query->select(['id', 'name']);
                        },
                    ])
                    ->find(Auth::user()->id)
                    ->getJwt();
            } else {
                $jwt = Auth::user()->getJwt();
            }

            return response()->json($jwt);
        }

        return response()->json('wrong_credentials', 422);
    }
}
