<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Returns available users.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return User::with('role')->get();
    }

    /**
     * Stores new user.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|unique:users,email',
                'name' => 'required',
                'password' => 'required',
                'password_conf' => 'required|same:password',
                'role_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();
            $user_data['password'] = bcrypt($user_data['password']);

            DB::transaction(function () use ($user_data) {
                User::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()
                ->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns single user.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $user = User::with('role')->find($id);

        if (is_null($user)) {
            return response()->json(null, 422);
        }

        return $user;
    }

    /**
     * Updates a user.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::with('role')->find($id);

        if (is_null($user)) {
            return response()->json(null, 422);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|unique:users,email,' . $id,
                'name' => 'required',
                'password' => 'sometimes|required',
                'password_conf' => 'required_with:password|same:password',
                'role_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('id');

            DB::transaction(function () use ($user, $user_data) {
                $user->update($user_data);
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a user.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::with('role')->find($id);

        if (is_null($user)) {
            return response()->json(null, 422);
        }

        try {
            DB::transaction(function () use ($user) {
                $user->delete();
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
