import Vue from 'vue';
import Vuex from 'vuex';
import LoggedUser from './LoggedUser';
import Role from './Role';
import User from './User';
import Service from './Service';
import Facility from './Facility';
import Season from './Season';
import Branch from './Branch';
import RoomType from './RoomType';
import Room from './Room';
import BedType from './BedType';
import Customer from './Customer';
import Reservation from './Reservation';
import PaymentStatus from './PaymentStatus';
import PaymentType from './PaymentType';
import ReservationStatus from './ReservationStatus';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    LoggedUser,
    Role,
    User,
    Service,
    Facility,
    Season,
    Branch,
    RoomType,
    Room,
    BedType,
    Customer,
    Reservation,
    PaymentStatus,
    PaymentType,
    ReservationStatus,
  },
});

export default store;
