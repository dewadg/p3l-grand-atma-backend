<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationServiceMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_service_map', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id');
            $table->foreign('service_id')
                ->references('id')->on('services');
            $table->string('payment_log_id');
            $table->foreign('payment_log_id')
                ->references('id')->on('payment_logs');
            $table->dateTime('date_time');
            $table->decimal('price', 15, 2);
            $table->smallInteger('qty');
            $table->decimal('subtotal', 15, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_service_map');
    }
}
