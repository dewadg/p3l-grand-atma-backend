<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Services\RoomService;
use App\RoomType;
use Carbon\Carbon;

class RoomTypeController extends Controller
{
    /**
     * Stores the service instance.
     *
     * @var RoomService;
     */
    protected $service;

    public function __construct()
    {
        $this->room_service = new RoomService;
    }

    /**
     * Returns available room types
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return RoomType::with([
            'facilities' => function ($query) {
                $query->select(['id', 'name']);
            },
            'rooms',
            'image',
        ])
            ->get()
            ->map(function ($item) {
                $item->price = $item->currentPrice();

                return $item;
            });
    }

    /**
     * Returns available room types for reservation.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function getForReservations(Request $request, $start, $end)
    {
        $validation = Validator::make(
            [
                'start' => $start,
                'end' => $end,
            ],
            [
                'start' => 'required|date',
                'end' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        return RoomType::with([
            'rooms' => function ($query) {
                $query->select(['id', 'smoking', 'bed_type_id', 'room_type_id']);
            },
            'rooms.bed' => function ($query) {
                $query->select(['id', 'name', 'capacity']);
            },
        ])
            ->has('rooms')
            ->get()
            ->map(function ($item) use ($start, $end) {
                $start_date = Carbon::createFromFormat('Y-m-d', $start);
                $end_date = Carbon::createFromFormat('Y-m-d', $end);

                $item->price = $item->currentPrice();

                $available_rooms = $this->room_service
                    ->availableRooms($start_date, $end_date, 0, $item->id)
                    ->map(function ($item) {
                        return $item->id;
                    });

                $item->available = $available_rooms->count();
                $item->image_url = is_null($item->image) ? '' : $item->image->url();

                $item->rooms->map(function ($item) use ($available_rooms) {
                    $item->available = in_array($item->id, $available_rooms->all());

                    return $item;
                });

                return $item;
            });
    }

    /**
     * Stores new room type.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'id' => 'required|max:2|unique:room_types,id',
                'name' => 'required',
                'description' => 'required',
                'size' => 'required',
                'price' => 'required',
                'capacity' => 'required',
                'image_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('facilities');
            $facilities = $request->get('facilities');

            DB::transaction(function () use ($user_data, $facilities) {
                $room_type = RoomType::create($user_data);
                $room_type->facilities()->sync($facilities);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Returns room type by ID.
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $room_type = RoomType::with([
            'facilities' => function ($query) {
                $query->select(['id', 'name']);
            },
            'image',
        ])
            ->find($id);

        if (is_null($room_type)) {
            return response()->json(null, 404);
        }

        if (! is_null($room_type->image)) {
            $room_type->image->url = $room_type->image->url();
        }

        return $room_type;
    }

    /**
     * Updates room type by its ID.
     *
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room_type = RoomType::find($id);

        if (is_null($room_type)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'description' => 'required',
                'size' => 'required',
                'price' => 'required',
                'capacity' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except(['id', 'facilities']);
            $facilities = $request->get('facilities');

            DB::transaction(function () use ($room_type, $user_data, $facilities) {
                $room_type->update($user_data);
                $room_type->facilities()->sync($facilities);
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Deletes room type by its ID
     *
     * @param string $id
     * @return void
     */
    public function destroy($id)
    {
        $room_type = RoomType::find($id);

        if (is_null($room_type)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($room_type) {
                $room_type->delete();
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response($e->getMessage(), 500);
        }
    }
}
