import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get('branches', successCallback, errorCallback);
    });
  },

  store(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        if (res.status === 201) {
          resolve();
        }
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.post('branches', payload, successCallback, errorCallback);
    });
  },

  fetch(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.get(`branches/${id}`, successCallback, errorCallback);
    });
  },

  update(context, payload) {
    return new Promise((resolve, reject) => {
      const { id } = payload;

      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.patch(`branches/${id}`, payload, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);
        reject(errData);
      };

      Http.delete(`branches/${id}`, successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
