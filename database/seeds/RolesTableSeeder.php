<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    private $data = [
        ['id' => 1, 'name' => 'Administrator'],
        ['id' => 2, 'name' => 'Operator'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $role) {
            Role::create($role);
        }
    }
}
