export default {
  namespaced: true,

  state: {
    data: {
      email: '',
      name: '',
    },
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  getters: {
    name: state => state.data.name,
    roleId: state => state.data.role_id,
  },
};
