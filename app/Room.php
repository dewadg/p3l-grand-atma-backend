<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Console\PackageDiscoverCommand;

class Room extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'room_type_id',
        'branch_id',
        'bed_type_id',
        'smoking',
    ];

    /**
     * Hidden attributes.
     *
     * @var array
     */
    protected $hidden = [
        'branch_id',
        'bed_type_id',
        'room_type_id',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'smoking' => 'boolean',
    ];

    /**
     * Set primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship to RoomType.
     *
     * @return RoomType
     */
    public function type()
    {
        return $this->belongsTo(RoomType::class, 'room_type_id');
    }

    /**
     * Relationship to Branch.
     *
     * @return Branch
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Relationship to BedType.
     *
     * @return BedType
     */
    public function bed()
    {
        return $this->belongsTo(BedType::class, 'bed_type_id');
    }
}
