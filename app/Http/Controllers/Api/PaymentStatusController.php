<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PaymentStatus;

class PaymentStatusController extends Controller
{
    /**
     * Returns available payment statuses.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return PaymentStatus::orderBy('name', 'ASC')->get();
    }
}
