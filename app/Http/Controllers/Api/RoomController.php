<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Room;

class RoomController extends Controller
{
    /**
     * Returns available rooms.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Room::with(['type', 'bed', 'branch'])->get();
    }

    /**
     * Stores new room.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'id' => 'required|max:9|unique:rooms,id',
                'room_type_id' => 'required',
                'branch_id' => 'required',
                'bed_type_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                Room::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns single room.
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $room = Room::with(['type', 'bed', 'branch'])
            ->find($id);

        if (is_null($room)) {
            return response()->json(null, 404);
        }

        return $room;
    }

    /**
     * Updates a room by its ID.
     *
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room = Room::find($id);

        if (is_null($room)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'bed_type_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->only(['bed_type_id', 'smoking']);

            DB::transaction(function () use ($room, $user_data) {
                $room->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes room by its ID.
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);

        if (is_null($room)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($room) {
                $room->delete();
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
