<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->string('id', 9);
            $table->primary('id');
            $table->string('room_type_id', 2);
            $table->foreign('room_type_id')
                ->references('id')->on('room_types');
            $table->string('branch_id', 3);
            $table->foreign('branch_id')
                ->references('id')->on('branches');
            $table->unsignedInteger('bed_type_id');
            $table->foreign('bed_type_id')
                ->references('id')->on('bed_types');
            $table->boolean('smoking')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
