import { required } from 'vuelidate/lib/validators';

export default {
  name: { required },
  phone: { required },
  address: { required },
  id_number: { required },
  type: { required },
};
