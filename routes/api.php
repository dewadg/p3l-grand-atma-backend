<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$config = ['namespace' => 'Api', 'middleware' => ['api', 'internal_jwt']];

Route::post('auth', 'Api\AuthController@authenticate');

Route::group($config, function () {
    Route::get('report-url', function () {
        return env('APP_URL') . '/report';
    });

    // Users
    Route::get('users', 'UserController@index');
    Route::post('users', 'UserController@store');
    Route::get('users/{id}', 'UserController@get');
    Route::patch('users/{id}', 'UserController@update');
    Route::delete('users/{id}', 'UserController@destroy');
    Route::get('roles', 'RoleController@index');

    // Services
    Route::get('services', 'ServiceController@index');
    Route::post('services', 'ServiceController@store');
    Route::get('services/{id}', 'ServiceController@get');
    Route::patch('services/{id}', 'ServiceController@update');
    Route::delete('services/{id}', 'ServiceController@destroy');

    // Facilities
    Route::get('facilities', 'FacilityController@index');
    Route::post('facilities', 'FacilityController@store');
    Route::get('facilities/{id}', 'FacilityController@get');
    Route::patch('facilities/{id}', 'FacilityController@update');
    Route::delete('facilities/{id}', 'FacilityController@destroy');

    // Seasons
    Route::get('seasons', 'SeasonController@index');
    Route::post('seasons', 'SeasonController@store');
    Route::get('seasons/{id}', 'SeasonController@get');
    Route::patch('seasons/{id}', 'SeasonController@update');
    Route::delete('seasons/{id}', 'SeasonController@destroy');
    Route::get('seasons/{id}/room-types', 'SeasonController@getRoomTypesPriceList');
    Route::post('seasons/{id}/room-types', 'SeasonController@saveRoomTypesPriceList');

    // Branches
    Route::get('branches', 'BranchController@index');
    Route::post('branches', 'BranchController@store');
    Route::get('branches/{id}', 'BranchController@get');
    Route::patch('branches/{id}', 'BranchController@update');
    Route::delete('branches/{id}', 'BranchController@destroy');

    // RoomTypes
    Route::get('room-types', 'RoomTypeController@index');
    Route::post('room-types', 'RoomTypeController@store');
    Route::get('room-types/{id}', 'RoomTypeController@get');
    Route::patch('room-types/{id}', 'RoomTypeController@update');
    Route::delete('room-types/{id}', 'RoomTypeController@destroy');

    // Rooms
    Route::get('bed-types', 'BedTypeController@index');
    Route::get('rooms', 'RoomController@index');
    Route::post('rooms', 'RoomController@store');
    Route::get('rooms/{id}', 'RoomController@get');
    Route::patch('rooms/{id}', 'RoomController@update');
    Route::delete('rooms/{id}', 'RoomController@destroy');

    // Customers
    Route::get('customers', 'CustomerController@index');
    Route::post('customers', 'CustomerController@store');
    Route::get('customers/{id}', 'CustomerController@get');
    Route::patch('customers/{id}', 'CustomerController@update');
    Route::delete('customers/{id}', 'CustomerController@destroy');

    // Reservations
    Route::get('payment-statuses', 'PaymentStatusController@index');
    Route::get('reservation-statuses', 'ReservationStatusController@index');
    Route::get('payment-types', 'PaymentTypeController@index');
    Route::get('reservations', 'ReservationController@index');
    Route::post('reservations', 'ReservationController@make');
    Route::get('reservations/available-rooms/{start}/{end}', 'RoomTypeController@getForReservations');
    Route::delete('reservations/{id}', 'ReservationController@destroy');
    Route::post('reservations/{id}/checkin', 'ReservationController@checkin');
    Route::post('reservations/{id}/checkout', 'ReservationController@checkout');

    // Reports
    Route::get('reports/monthly-incomes/{year}', 'ReportController@monthlyIncomes');

    // Images
    Route::post('images', 'ImageController@upload');
    Route::delete('images/{id}', 'ImageController@destroy');
});
