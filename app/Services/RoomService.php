<?php

namespace App\Services;

use Carbon\Carbon;
use App\ReservationRoom;
use App\Room;

class RoomService
{
    /**
     * Returns available room for selected date
     * or room type ID (if specified)
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param int $limit
     * @param int $room_type_id
     * @return Illuminate\Support\Collection
     */
    public function availableRooms(Carbon $start, Carbon $end, $limit = 0, $room_type_id = null, $smoking = false)
    {
        $occupied_rooms = ReservationRoom::with('reservation')
            ->where(function ($query) use ($start, $end) {
                $query->whereBetween('start', [$start, $end])
                    ->orWhereBetween('end', [$start, $end]);
            })
            ->get()
            ->filter(function ($item) {
                // Exclude reservation status: check-out, canceled.
                $excluded = [3, 5];

                return ! in_array($item->reservation->reservation_status_id, $excluded);
            })
            ->map(function ($item) {
                return $item->room_id;
            });

        return Room::with('type')
            ->whereNotIn('id', $occupied_rooms)
            ->when(! is_null($room_type_id), function ($query) use ($room_type_id) {
                return $query->where('room_type_id', $room_type_id);
            })
            ->when($smoking, function ($query) {
                return $query->where('smoking', 1);
            })
            ->when($limit !== 0, function ($query) use ($limit) {
                return $query->take($limit);
            })
            ->orderBy('id', 'ASC')
            ->get()
            ->map(function ($item) {
                $item->price = $item->type->currentPrice();

                return $item;
            });
    }
}
