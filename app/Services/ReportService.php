<?php

namespace App\Services;

use Carbon\Carbon;
use App\Customer;
use App\Reservation;
use App\RoomType;

class ReportService
{
    /**
     * Returns registered customers on the year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function customers($year)
    {
        $months = [
            '01' => ['name' => 'Januari', 'count' => 0],
            '02' => ['name' => 'Februari', 'count' => 0],
            '03' => ['name' => 'Maret', 'count' => 0],
            '04' => ['name' => 'April', 'count' => 0],
            '05' => ['name' => 'Mei', 'count' => 0],
            '06' => ['name' => 'Juni', 'count' => 0],
            '07' => ['name' => 'Juli', 'count' => 0],
            '08' => ['name' => 'Agustus', 'count' => 0],
            '09' => ['name' => 'September', 'count' => 0],
            '10' => ['name' => 'Oktober', 'count' => 0],
            '11' => ['name' => 'November', 'count' => 0],
            '12' => ['name' => 'Desember', 'count' => 0],
        ];
        $total = 0;

        Customer::get()
            ->groupBy(function ($item) {
                return $item->created_at->format('m');
            })
            ->each(function ($item, $key) use (&$months, &$total) {
                $count = $item->count();
                $months[$key]['count'] = $count;
                $total += $count;
            });

        return [
            'year' => $year,
            'total' => $total,
            'data' => $months,
        ];
    }

    /**
     * Returns count of customers on available room types.
     *
     * @param string $year
     * @param string $month
     * @return Illuminate\Http\Response
     */
    public function roomTypes($year, $month)
    {
        $total = 0;
        $room_types = RoomType::get(['id', 'name'])
            ->map(function ($room_type) use ($year, $month, &$total) {
                $reservations = Reservation::whereYear('created_at', $year)
                    ->whereMonth('created_at', $month)
                    ->get()
                    ->map(function ($reservation) {
                        $reservation->room_type_ids = $reservation->rooms->map(function ($room) {
                            return $room->room_type_id;
                        })
                            ->all();

                        return $reservation;
                    })
                    ->filter(function ($reservation) use ($room_type) {
                        return in_array($room_type->id, $reservation->room_type_ids);
                    })
                    ->groupBy('type');

                $room_type->personals = isset($reservations['P']) ? $reservations['P']->count() : 0;
                $room_type->groups = isset($reservations['G']) ? $reservations['G']->count() : 0;
                $room_type->total = $room_type->personals + $room_type->total;
                $total += $room_type->total;

                return $room_type;
            });

        return [
            'year' => $year,
            'month' => Carbon::createFromFormat('m', $month)->format('F'),
            'total' => $total,
            'data' => $room_types,
        ];
    }

    /**
     * Returns top customers by transaction.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function topCustomers($year)
    {
        $total = 0;
        $customers = Reservation::with(['customer'])
            ->whereYear('created_at', $year)
            ->get()
            ->groupBy('employee_id')
            ->map(function ($item) use (&$total) {
                $subtotal = $item->sum('total');
                $total += $subtotal;

                return [
                    'name' => $item[0]->customer->name,
                    'reservations' => $item->count(),
                    'total' => $subtotal,
                ];
            })
            ->sortByDesc(function ($item) {
                return $item['total'];
            })
            ->take(5)
            ->all();

        return [
            'year' => $year,
            'total' => $total,
            'data' => $customers,
        ];
    }

    /**
     * Returns each months incomes by year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function monthlyIncomes($year)
    {
        $total = 0;
        $months = collect([
            '01' => ['name' => 'Januari'],
            '02' => ['name' => 'Februari'],
            '03' => ['name' => 'Maret'],
            '04' => ['name' => 'April'],
            '05' => ['name' => 'Mei'],
            '06' => ['name' => 'Juni'],
            '07' => ['name' => 'Juli'],
            '08' => ['name' => 'Agustus'],
            '09' => ['name' => 'September'],
            '10' => ['name' => 'Oktober'],
            '11' => ['name' => 'November'],
            '12' => ['name' => 'Desember'],
        ])
            ->map(function ($month, $key) use ($year, &$total) {
                $reservations = Reservation::whereYear('created_at', $year)
                    ->whereMonth('created_at', $key)
                    ->get();

                $num_of_reservations = $reservations->count();
                $reservations = $reservations->groupBy('type');
                $personal = isset($reservations['P']) ? $reservations['P']->sum('total') : 0;
                $group = isset($reservations['G']) ? $reservations['G']->sum('total') : 0;
                $subtotal = $personal + $group;
                $total += $subtotal;

                return [
                    'name' => $month['name'],
                    'reservations' => $num_of_reservations,
                    'personal' => $personal,
                    'group' => $group,
                    'total' => $subtotal,
                ];
            });

        return [
            'year' => $year,
            'total' => $total,
            'data' => $months,
        ];
    }

    /**
     * Returns each branches' incomes by year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function yearlyIncomesPerBranches($year)
    {
        $now = Carbon::createFromFormat('Y', $year);
        $years = collect([]);
        $total = 0;

        for ($start = $now->copy()->subYears(4); $start->lte($now); $start->addYear()) {
            $years->push($start->format('Y'));
        }

        $years = $years->map(function ($year) use (&$total) {
            $reservations = Reservation::with('branch')
                ->whereYear('created_at', $year)
                ->get()
                ->groupBy('branch_id');

            $jog = isset($reservations['JOG']) ? $reservations['JOG']->sum('total') : 0;
            $bdg = isset($reservations['BDG']) ? $reservations['BDG']->sum('total') : 0;
            $subtotal = $jog + $bdg;
            $total += $subtotal;

            return [
                'year' => $year,
                'jog' => $jog,
                'bdg' => $bdg,
                'total' => $subtotal,
            ];
        });

        return [
            'year' => $year,
            'total' => $total,
            'data' => $years,
        ];
    }
}
