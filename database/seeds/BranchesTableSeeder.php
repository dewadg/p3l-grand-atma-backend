<?php

use Illuminate\Database\Seeder;
use App\Branch;

class BranchesTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 'JOG',
            'name' => 'Cabang Yogyakarta',
            'phone' => '02742883477',
            'address' => 'Jl. Malioboro',
        ],
        [
            'id' => 'BDG',
            'name' => 'Cabang Bandung',
            'phone' => '02732883477',
            'address' => 'Jl. Asia-Afrika',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $branch) {
            Branch::create($branch);
        }
    }
}
