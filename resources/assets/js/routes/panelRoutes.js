import Vue from 'vue';
import VueRouter from 'vue-router';
import middleware from './middleware';

import LoginView from '../components/Login/LoginView';
import DashboardView from '../components/Dashboard/DashboardView';

import UserIndex from '../components/User/UserIndex';
import ServiceIndex from '../components/Service/ServiceIndex';
import FacilityIndex from '../components/Facility/FacilityIndex';
import SeasonIndex from '../components/Season/SeasonIndex';
import BranchIndex from '../components/Branch/BranchIndex';
import RoomTypeIndex from '../components/RoomType/RoomTypeIndex';
import RoomIndex from '../components/Room/RoomIndex';
import PriceListIndex from '../components/PriceList/PriceListIndex';
import CustomerIndex from '../components/Customer/CustomerIndex';
import ReservationIndex from '../components/Reservation/ReservationIndex';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { name: 'login', path: '/', component: LoginView },
    { name: 'dashboard', path: '/dashboard', component: DashboardView, beforeEnter: middleware.auth },
    { name: 'users.index', path: '/users', component: UserIndex, beforeEnter: middleware.auth },
    { name: 'services.index', path: '/services', component: ServiceIndex, beforeEnter: middleware.auth },
    { name: 'facilities.index', path: '/facilities', component: FacilityIndex, beforeEnter: middleware.auth },
    { name: 'seasons.index', path: '/seasons', component: SeasonIndex, beforeEnter: middleware.auth },
    { name: 'branches.index', path: '/branches', component: BranchIndex, beforeEnter: middleware.auth },
    { name: 'room_types.index', path: '/room-types', component: RoomTypeIndex, beforeEnter: middleware.auth },
    { name: 'rooms.index', path: '/rooms', component: RoomIndex, beforeEnter: middleware.auth },
    { name: 'price_list', path: '/price-list', component: PriceListIndex, beforeEnter: middleware.auth },
    { name: 'customers.index', path: '/customers', component: CustomerIndex, beforeEnter: middleware.auth },
    { name: 'reservations.index', path: '/reservations', component: ReservationIndex, beforeEnter: middleware.auth },
  ],
});

export default router;
