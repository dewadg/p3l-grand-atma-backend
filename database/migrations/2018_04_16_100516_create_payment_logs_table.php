<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->string('reservation_id');
            $table->foreign('reservation_id')
                ->references('id')->on('reservations');
            $table->unsignedInteger('payment_type_id');
            $table->foreign('payment_type_id')
                ->references('id')->on('payment_types');
            $table->unsignedInteger('credit_card_id')->nullable();
            $table->foreign('credit_card_id')
                ->references('id')->on('credit_cards');
            $table->unsignedInteger('payment_status_id');
            $table->foreign('payment_status_id')
                ->references('id')->on('payment_statuses');
            $table->dateTime('date_time');
            $table->decimal('total', 15, 2);
            $table->decimal('paid', 15, 2)->nullable();
            $table->decimal('change', 15, 2)->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_logs');
    }
}
