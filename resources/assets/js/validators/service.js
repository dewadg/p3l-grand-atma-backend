import {
  required,
  minValue,
} from 'vuelidate/lib/validators';

export default {
  name: { required },
  price: {
    required,
    minValue: minValue(1000),
  },
};
