<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ReservationRoom extends Pivot
{
    /**
     * Sets the table.
     *
     * @var string
     */
    protected $table = 'reservation_room_map';

    /**
     * Disable timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'double',
        'subtotal' => 'double',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
    ];

    /**
     * Relationship to Reservation.
     *
     * @return Reservation
     */
    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }
}
