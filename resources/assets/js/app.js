import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuelidate from 'vuelidate';
import moment from 'moment';
import Dropzone from 'dropzone';
import Http from './services/Http';
import App from './components/App';
import router from './routes/panelRoutes';
import store from './stores';

Vue.use(Vuetify);
Vue.use(Vuelidate);

moment.locale('id');
Http.init();

Dropzone.autoDiscover = false;

new Vue({
  router,
  store,

  render(h) {
    return h(App);
  },

  async mounted() {
    await this.$store.dispatch('Role/fetchAll');
  },
})
  .$mount('#app');
