import Vue from 'vue';
import Vuelidate from 'vuelidate';
import moment from 'moment';
import Http from './services/Http';
import Home from './components/Home';
import router from './routes/homeRoutes';
import store from './stores';

Vue.use(Vuelidate);

moment.locale('id');
Http.init();

new Vue({
  router,
  store,

  render(h) {
    return h(Home);
  },
})
  .$mount('#app');
