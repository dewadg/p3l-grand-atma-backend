<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentLog extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'reservation_id',
        'payment_type_id',
        'credit_card_id',
        'payment_status_id',
        'date_time',
        'total',
        'paid',
        'change',
        'description',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'total' => 'double',
        'paid' => 'double',
        'change' => 'double',
    ];

    /**
     * Sets the primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Date-casted atrributes.
     *
     * @var array
     */
    protected $dates = [
        'date_time',
        'deleted_at',
    ];

    /**
     * Relationship to PaymentType.
     *
     * @return PaymentType
     */
    public function type()
    {
        return $this->belongsTo(PaymentType::class);
    }

    /**
     * Relationship to CreditCard.
     *
     * @return CreditCard
     */
    public function creditCard()
    {
        return $this->belongsTo(CreditCard::class);
    }

    /**
     * Relationship to PaymentStatus.
     *
     * @return PaymentStatus
     */
    public function status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    /**
     * Relationship to ReservationRoom.
     *
     * @return array
     */
    public function rooms()
    {
        return $this->hasMany(ReservationRoom::class);
    }

    /**
     * Relationship to ReservationService.
     *
     * @return void
     */
    public function services()
    {
        return $this->hasMany(ReservationService::class);
    }

    /**
     * Generates unique ID.
     *
     * @return string
     */
    public static function generateId($is_group = false)
    {
        $type = $is_group ? 'G' : 'P';
        $index = self::withTrashed()->get()->count() + 1;

        return $type
            . date('d')
            . date('m')
            . date('y')
            . '-'
            . str_pad((string)$index, 3, '0', STR_PAD_LEFT);
    }
}
