<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;

class RoleController extends Controller
{
    /**
     * Returns available roles.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Role::get();
    }
}
