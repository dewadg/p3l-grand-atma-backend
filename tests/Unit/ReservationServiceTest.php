<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use App\Services\ReservationService;
use App\Customer;

class ReservationServiceTest extends TestCase
{
    /**
     * Stores the service instance.
     *
     * @var ReservationService
     */
    protected $service;

    public function __construct()
    {
        parent::__construct();

        $this->service = new ReservationService;
    }

    public function testMake()
    {      
        $start = Carbon::now();
        $end = $start->copy()->addDays(2);
        $admin_data = [
            'branch_id' => 'JOG',
            'employee_id' => 1,
            'reservation_status_id' => 1,
            'payment_type_id' => 2,
            'payment_status_id' => 1,
            'dp' => 0,
            'paid' => null,
        ];
        $customer_data = [
            'customer_id' => 1,
            'num_of_adults' => 2,
            'num_of_kids' => 0,
            'additional_request' => 'Silent room',
            'room_types' => [
                ['id' => 'ED', 'qty' => 1, 'smoking' => true],
                ['id' => 'JS', 'qty' => 1, 'smoking' => true],
            ],
        ];

        $this->assertNotNull($this->service->make(
            $start,
            $end,
            $admin_data,
            $customer_data
        ));
    }
}
