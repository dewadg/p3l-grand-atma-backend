import { required } from 'vuelidate/lib/validators';

export default {
  id: { required },
  floor: { required },
  room: { required },
  branch_id: { required },
  room_type_id: { required },
  bed_type_id: { required },
};
