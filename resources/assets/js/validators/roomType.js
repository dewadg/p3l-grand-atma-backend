import {
  required,
  minLength,
  minValue,
} from 'vuelidate/lib/validators';

export default {
  id: {
    required,
    minLength: minLength(2),
  },
  name: { required },
  description: { required },
  size: { required },
  capacity: {
    required,
    minValue: minValue(1),
  },
  price: {
    required,
    minValue: minValue(1000),
  },
  facilities: {
    required,
    minLength: minLength(1),
  },
  image_id: {
    required,
  },
};
