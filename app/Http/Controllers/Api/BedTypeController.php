<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\BedType;

class BedTypeController extends Controller
{
    /**
     * Returns available bed types.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return BedType::get();
    }
}
