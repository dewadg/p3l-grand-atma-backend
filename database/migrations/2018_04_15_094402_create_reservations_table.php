<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->unsignedInteger('employee_id')->nullable();
            $table->foreign('employee_id')
                ->references('id')->on('employees');
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')
                ->references('id')->on('customers');
            $table->string('branch_id', 5);
            $table->foreign('branch_id')
                ->references('id')->on('branches');
            $table->unsignedInteger('reservation_status_id');
            $table->foreign('reservation_status_id')
                ->references('id')->on('reservation_statuses');
            $table->enum('type', ['P', 'G']);
            $table->date('start');
            $table->date('end');
            $table->smallInteger('duration');
            $table->smallInteger('num_of_adults');
            $table->smallInteger('num_of_kids');
            $table->decimal('dp', 15, 2)->nullable();
            $table->decimal('deposit', 15, 2)->nullable();
            $table->decimal('total', 15, 2);
            $table->boolean('smoking_rooms')->default(false);
            $table->text('additional_request')->nullable();
            $table->dateTime('checkin_at')->nullable();
            $table->dateTime('checkout_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
