<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PaymentType;

class PaymentTypeController extends Controller
{
    /**
     * Returns available payment types.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return PaymentType::orderBy('name', 'ASC')->get();
    }
}
