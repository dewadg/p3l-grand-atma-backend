<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'phone',
        'address',
        'id_number',
        'type',
        'user_id',
    ];

    /**
     * Relationship to User.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relationship to CreditCard.
     *
     * @return array
     */
    public function creditCards()
    {
        return $this->hasMany(CreditCard::class);
    }
}
