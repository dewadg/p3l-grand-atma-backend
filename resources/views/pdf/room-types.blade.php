@extends('pdf.master')
@section('style')
  .year {
    display: block;
    margin-bottom: 15px;
  }

  .report {
    width: 100%;
    border-collapse: collapse;
  }

  .report thead tr th,
  .report tbody tr td,
  .report tfoot tr td {
    padding: 5px;
  }

  .report tfoot tr td {
    font-weight: bold;
  }
@endsection
@section('content')
  <h2 class="text-center">LAPORAN JUMLAH TAMU</h2>
  <span class="year">Tahun: {{ $year }}</span>
  <span class="year">Bulan: {{ $month }}</span>
  <table class="report" border="1">
    <thead>
      <tr>
        <th class="text-center" width="50">No.</th>
        <th class="text-center">Jenis Kamar</th>
        <th class="text-center" width="75">Grup</th>
        <th class="text-center" width="75">Personal</th>
        <th class="text-center" width="75">Total</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $row)
        <tr>
          <td class="text-center">{{ ($loop->index + 1) }}</td>
          <td>{{ $row->name }}</td>
          <td class="text-right">{{ $row->groups }}</td>
          <td class="text-right">{{ $row->personals }}</td>
          <td class="text-right">{{ $row->total }}</td>
        </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td class="text-right" colspan="4">TOTAL</td>
        <td class="text-right">{{ $total }}</td>
      </tr>
    </tfoot>
  </table>
  <p class="text-right">Dicetak pada {{ date('j F Y H:i') }}</p>
@endsection
