import { required } from 'vuelidate/lib/validators';

export default {
  name: { required },
  start: { required },
  end: { required },
};
