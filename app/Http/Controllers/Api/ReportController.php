<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ReportService;

class ReportController extends Controller
{
    /**
     * Stores the report service instance.
     *
     * @var ReportService
     */
    protected $service;

    public function __construct()
    {
        $this->service = new ReportService;
    }

    /**
     * Returns monthly incomes by year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function monthlyIncomes($year)
    {
        return $this->service->monthlyIncomes($year)['data']->values();
    }
}
