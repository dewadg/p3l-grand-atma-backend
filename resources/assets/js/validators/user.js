import {
  required,
  email,
} from 'vuelidate/lib/validators';

export default {
  name: { required },
  email: {
    required,
    email,
  },
  role_id: { required },
};
