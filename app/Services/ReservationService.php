<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Customer;
use App\Reservation;
use App\ReservationRoom;
use App\ReservationService as ReservationServicePivot;
use App\RoomType;

class ReservationService
{
    /**
     * Stores the RoomService instance.
     *
     * @var RoomService
     */
    protected $room_service;

    /**
     * Stores the PaymentService instance.
     *
     * @var PaymentService
     */
    protected $payment_service;

    public function __construct()
    {
        $this->room_service = new RoomService;
        $this->payment_service = new PaymentService;
    }

    /**
     * Prepares Room based on given RoomType data.
     *
     * @param array $room_types_data
     * @return array
     */
    protected function prepareRooms($room_types_data)
    {
        $ids = collect($room_types_data)->map(function ($item, $key) {
            return $key;
        });
    }

    /**
     * Make reservation.
     *
     * @param Carbon $start
     * @param Carbon $end
     * @param array $admin_data
     * @param array $customer_data
     * @return void
     */
    public function make(
        Carbon $start,
        Carbon $end,
        $admin_data,
        $customer_data
    ) {
        $customer = Customer::find($customer_data['customer_id']);
        $duration = $start->diffInDays($end);
        $reserved_rooms = collect([]);
        $reserved_services = collect([]);

        // Preparing rooms
        foreach ($customer_data['rooms'] as $room) {
            $available_rooms = $this->room_service->availableRooms($start, $end)
                ->map(function ($item) {
                    return $item->id;
                })
                ->all();

            if (! in_array($room['id'], $available_rooms)) {
                throw new \Exception('room_not_available');
            }

            $room['subtotal'] = $room['price'] * $duration;
            $reserved_rooms->push($room);
        }

        // Preparing services
        foreach ($customer_data['services'] as $service) {
            $service['subtotal'] = $service['price'] * $service['qty'];
            $reserved_services->push($service);
        }

        // Calculating total
        $total = $reserved_rooms->sum('subtotal') + $reserved_services->sum('subtotal');
        $dp = $admin_data['reservation_status_id'] == 1 || $admin_data['reservation_status_id'] == 2
            ? $total / 2
            : null;

        if ($admin_data['payment_type_id'] == 1 || $admin_data['payment_type_id'] == 2) {
            $paid = $total;
        } else {
            $paid = $admin_data['paid'];
        }

        try {
            DB::beginTransaction();
            
            $reservation = Reservation::create([
                'id' => Reservation::generateId(),
                'type' => $customer->type,
                'employee_id' => $admin_data['employee_id'],
                'customer_id' => $customer->id,
                'reservation_status_id' => $admin_data['reservation_status_id'],
                'branch_id' => $admin_data['branch_id'],
                'start' => $start,
                'end' => $end,
                'duration' => $duration,
                'num_of_adults' => $customer_data['num_of_adults'],
                'num_of_kids' => $customer_data['num_of_kids'],
                'dp' => $dp,
                'total' => $total,
                'additional_request' => $customer_data['additional_request'],
                'checkin' => $admin_data['reservation_status_id'] === 4 ? Carbon::now() : null,
                'deposit' => $admin_data['reservation_status_id'] == 4 ? $admin_data['deposit'] : null,
            ]);

            if (
                $admin_data['reservation_status_id'] == 1 ||
                $admin_data['reservation_status_id'] == 2
            ) {
                // If transaction needs DP.

                // DP.
                $payment_log = $this->payment_service->make(
                    $admin_data['payment_type_id'],
                    $admin_data['payment_status_id'],
                    $reservation,
                    $customer,
                    $dp,
                    $dp,
                    'DP reservasi ' . $reservation->id,
                    $customer_data['credit_card_id']
                );

                // Total - DP.
                $payment_log = $this->payment_service->make(
                    $admin_data['payment_type_id'],
                    1,
                    $reservation,
                    $customer,
                    $total - $dp,
                    null,
                    'Tagihan ' . $reservation->id,
                    $customer_data['credit_card_id']
                );
            } else {
                // If transaction doesn't need DP.
                $payment_log = $this->payment_service->make(
                    $admin_data['payment_type_id'],
                    $admin_data['payment_status_id'],
                    $reservation,
                    $customer,
                    $total,
                    $paid,
                    'Tagihan reservasi ' . $reservation->id,
                    $customer_data['credit_card_id']
                );
            }

            $reserved_rooms->each(function ($item) use (
                &$reservation,
                $payment_log,
                $start,
                $end
            ) {
                ReservationRoom::create([
                    'reservation_id' => $reservation->id,
                    'room_id' => $item['id'],
                    'payment_log_id' => $payment_log->id,
                    'start' => $start,
                    'end' => $end,
                    'price' => $item['price'],
                    'subtotal' => $item['subtotal'],
                ]);
            });

            $now = Carbon::now();

            $reserved_services->each(function ($item) use (
                &$reservation,
                $payment_log,
                $start,
                $end,
                $now
            ) {
                ReservationServicePivot::create([
                    'reservation_id' => $reservation->id,
                    'service_id' => $item['id'],
                    'payment_log_id' => $payment_log->id,
                    'price' => $item['price'],
                    'qty' => $item['qty'],
                    'subtotal' => $item['subtotal'],
                    'date_time' => $now,
                ]);
            });

            DB::commit();

            return $reservation;
        } catch (\Exception $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * Do checkin on a reservation.
     *
     * @param Reservation $reservation
     * @param double $deposit
     * @return void
     */
    public function checkin(Reservation $reservation, $deposit)
    {
        if ($reservation->reservation_status_id == 4 || ! is_null($reservation->checkin)) {
            throw new \Exception('The guest already checked-in');
        }

        try {
            DB::transaction(function () use ($reservation, $deposit) {
               $reservation->update([
                   'reservation_status_id' => 4,
                   'checkin_at' => Carbon::now(),
                   'deposit' => $deposit,
               ]);
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Performs checkout on a reservation
     *
     * @param Reservation $reservation
     * @param int $payment_type_id
     * @return void
     */
    public function checkout(
        Reservation $reservation,
        $payment_type_id,
        $paid = null
    ) {
        if ($reservation->reservation_status_id == 5) {
            throw new \Exception('The guest already checked-out');
        }

        try {
            DB::transaction(function () use (
                $reservation,
                $payment_type_id
            ) {
                $unpaid_payment_logs = $reservation->paymentLogs
                    ->filter(function ($item) {
                        return $item->payment_status_id == 1;
                    });

                if ($unpaid_payment_logs->count() > 0) {
                    if ($payment_type_id == 1) {
                        $unpaid_payment_logs->each(function ($item) use ($payment_type_id) {
                            $item->update([
                                'paid' => $item->total,
                                'payment_type_id' => $payment_type_id,
                                'payment_status_id' => 2,
                            ]);
                        });
                    }
                }

                $reservation->update([
                    'reservation_status_id' => 5,
                    'checkout_at' => Carbon::now(),
                ]);
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
