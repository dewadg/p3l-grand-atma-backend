<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class JwtGenerationTest extends TestCase
{
    public function testGetJwt()
    {
        $user = User::with(['role', 'employee', 'employee.branch'])->find(1);

        $this->assertTrue(is_string($user->getJwt()));
    }
}
