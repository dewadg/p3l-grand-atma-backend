import jwt from 'jsonwebtoken';
import Cookies from 'js-cookie';
import Http from './Http';
import store from '../stores';

export default {
  authenticate(email, password) {
    return new Promise((resolve, reject) => {
      const payload = {
        email,
        password,
      };

      const successCallback = (res) => {
        if (res.status === 200) {
          const token = res.data;
          const user = this.decodeJwt(token);

          this.storeJwt(token);
          Http.setJwt(token);
          store.commit('LoggedUser/setSource', user);
          resolve();
        }
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.post('auth', payload, successCallback, errorCallback);
    });
  },

  storeJwt(token) {
    Cookies.set('jwt', token);
  },

  getJwt() {
    return Cookies.get('jwt');
  },

  decodeJwt(token) {
    const decodedToken = jwt.decode(token, { complete: true });

    return decodedToken.payload.user;
  },

  clearJwt() {
    Cookies.remove('jwt');
  },

  refetchToken() {
    const token = this.getJwt();
    const user = this.decodeJwt(token);

    Http.setJwt(token);
    store.commit('LoggedUser/setSource', user);
  },
};
