<?php

namespace App\Services;

use Carbon\Carbon;
use App\PaymentLog;
use App\Customer;
use App\Reservation;

class PaymentService
{
    /**
     * Make a PaymentLog.
     *
     * @param int $type
     * @param int $status
     * @param Reservation $reservation
     * @param Customer $customer
     * @param double $total
     * @param double $paid
     * @param string $description
     * @param int $credit_card_id
     * @return PaymentLog
     */
    public function make(
        $type,
        $status,
        Reservation $reservation,
        Customer $customer,
        $total,
        $paid,
        $description,
        $credit_card_id = null
    ) {
        return PaymentLog::create([
            'id' => PaymentLog::generateId(),
            'reservation_id' => $reservation->id,
            'payment_type_id' => $type,
            'credit_card_id' => $type === 1 ? $credit_card_id : null,
            'date_time' => Carbon::now(),
            'total' => $total,
            'paid' => $paid,
            'change' => is_null($paid) ? null : $paid - $total,
            'payment_status_id' => $status,
            'description' => $description,
        ]);
    }
}
