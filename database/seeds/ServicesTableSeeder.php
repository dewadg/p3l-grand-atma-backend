<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesTableSeeder extends Seeder
{
    private $data = [
        ['id' => 1, 'name' => 'Extra Bed', 'price' => 150000],
        ['id' => 2, 'name' => 'Laundry Regular / potong', 'price' => 10000],
        ['id' => 3, 'name' => 'Laundry Fast Service / potong', 'price' => 25000],
        ['id' => 4, 'name' => 'Massage / orang', 'price' => 75000],
        ['id' => 5, 'name' => 'Minibar / minuman', 'price' => 20000],
        ['id' => 6, 'name' => 'Tambahan breakfast / orang ', 'price' => 50000],
        ['id' => 7, 'name' => 'Lunch package / orang', 'price' => 100000],
        ['id' => 8, 'name' => 'Dinner package / orang', 'price' => 100000],
        ['id' => 9, 'name' => 'Meeting Room Full Day / orang', 'price' => 200000],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $service) {
            Service::create($service);
        }
    }
}
