<?php

use Illuminate\Database\Seeder;
use App\Facility;

class FacilitiesTableSeeder extends Seeder
{
    private $data = [
        ['id' => 1, 'name' => 'AC'],
        ['id' => 2, 'name' => 'Air minum kemasan gratis'],
        ['id' => 3, 'name' => 'Brankas dalam kamar'],
        ['id' => 4, 'name' => 'Fasilitas membuat kopi/teh'],
        ['id' => 5, 'name' => 'Jubah Mandi'],
        ['id' => 6, 'name' => 'Layanan kamar (24jam)'],
        ['id' => 7, 'name' => 'Meja Tulis'],
        ['id' => 8, 'name' => 'Minibar'],
        ['id' => 9, 'name' => 'Pembersihan kamar harian'],
        ['id' => 10, 'name' => 'Pengering Rambut'],
        ['id' => 11, 'name' => 'Peralatan mandi gratis'],
        ['id' => 12, 'name' => 'Sandal'],
        ['id' => 13, 'name' => 'Telepon'],
        ['id' => 14, 'name' => 'Tempat tidur ekstra'],
        ['id' => 15, 'name' => 'Tempat tidur premium'],
        ['id' => 16, 'name' => 'Tirai kedap-cahaya'],
        ['id' => 17, 'name' => 'TV Kabel'],
        ['id' => 18, 'name' => 'TV LCD'],
        ['id' => 19, 'name' => 'WiFi Gratis'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $facility) {
            Facility::create($facility);
        }
    }
}
