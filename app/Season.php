<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Season extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'start',
        'end',
    ];

    /**
     * Date casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'start',
        'end',
    ];

    /**
     * Relationship to RoomType.
     *
     * @return array
     */
    public function roomTypes()
    {
        return $this->belongsToMany(RoomType::class, 'room_type_season_map')
            ->withPivot('price');
    }
}
