<?php

use Illuminate\Database\Seeder;
use App\ReservationStatus;

class ReservationStatusesTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    private $data = [
        ['id' => 1, 'name' => 'Menunggu Pembayaran'],
        ['id' => 2, 'name' => 'Dibayar'],
        ['id' => 3, 'name' => 'Dibatalkan'],
        ['id' => 4, 'name' => 'Check-in'],
        ['id' => 5, 'name' => 'Check-out'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $reservation_status) {
            ReservationStatus::create($reservation_status);
        }
    }
}
