<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Season;
use App\RoomType;

class SeasonController extends Controller
{
    /**
     * Returns available seasons.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Season::get();
    }

    /**
     * Stores new service.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'start' => 'required|date',
                'end' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                Season::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns single service.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $service = Season::find($id);

        if (is_null($service)) {
            return response()->json(null, 404);
        }

        return $service;
    }

    /**
     * Updates a service
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Season::find($id);

        if (is_null($service)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'start' => 'required|date',
                'end' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('id');

            DB::transaction(function () use ($service, $user_data) {
                $service->update($user_data);
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a service
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Season::find($id);

        if (is_null($service)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($service) {
                $service->delete();
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function getRoomTypesPriceList($id)
    {
        $season = Season::find($id);

        if (is_null($season)) {
            return response()->json(null, 404);
        }

        $available_room_types = [];
        $room_types = $season->roomTypes()
            ->get()
            ->map(function ($item) use (&$available_room_types) {
                $data = new \StdClass;

                $data->id = $item->id;
                $data->name = $item->name;
                $data->initial_price = $item->price;
                $data->price = doubleval($item->pivot->price);

                array_push($available_room_types, $item->id);

                return $data;
            })
            ->all();

        RoomType::get(['id', 'name', 'price'])
            ->each(function ($item) use (
                &$available_room_types,
                &$room_types
            ) {
                if (in_array($item->id, $available_room_types)) {
                    return;
                }

                $item->initial_price = $item->price;
                $item->price = 0;

                array_push($room_types, $item);
            });

        return $room_types;
    }

    public function saveRoomTypesPriceList(Request $request, $id)
    {
        $season = Season::find($id);

        if (is_null($season)) {
            return response()->json(null, 404);
        }

        try {
            $user_data = [];

            collect($request->get('price_list'))
                ->each(function ($item) use (&$user_data) {
                    $user_data[$item['id']] = ['price' => $item['price']];
                });

            DB::transaction(function () use ($season, $user_data) {
                $season->roomTypes()->sync($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
