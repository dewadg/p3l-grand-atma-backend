import Vue from 'vue';
import VueRouter from 'vue-router';
// import middleware from './middleware';

import HomeIndex from '../components/Home/HomeIndex';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { name: 'index', path: '/', component: HomeIndex },
  ],
});

export default router;
