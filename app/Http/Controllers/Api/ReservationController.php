<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Services\ReservationService;
use App\Reservation;
use Carbon\Carbon;

class ReservationController extends Controller
{
    /**
     * Stores the service instance.
     *
     * @var ReservationService
     */
    protected $service;

    public function __construct()
    {
        $this->service = new ReservationService;
    }

    /**
     * Returns available reservations.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Reservation::with([
            'employee',
            'customer',
            'branch',
            'status',
            'rooms',
            'rooms.type',
            'services',
            'paymentLogs',
            'paymentLogs.type',
        ])
            ->get();
    }

    /**
     * Makes new reservation
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function make(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'reservation_status_id' => 'required',
                'customer_id' => 'required',
                'start' => 'required|date',
                'end' => 'required|date',
                'num_of_adults' => 'required',
                'rooms' => 'required|array',
                'services' => 'array',
                'payment_type_id' => 'required',
                'payment_status_id' => 'required',
            ]
        );

        $start = Carbon::createFromFormat('Y-m-d', $request->get('start'));
        $end = Carbon::createFromFormat('Y-m-d', $request->get('end'));

        $admin_data = [
            'branch_id' => $request->user->employee->branch->id,
            'employee_id' => $request->user->employee->id,
            'reservation_status_id' => $request->get('reservation_status_id'),
            'payment_type_id' => $request->get('payment_type_id'),
            'payment_status_id' => $request->get('payment_status_id'),
            'dp' => $request->get('dp'),
            'paid' => $request->get('paid') == 0 ? null : $request->get('paid'),
            'deposit' => $request->get('deposit'),
        ];

        $customer_data = [
            'customer_id' => $request->get('customer_id'),
            'num_of_adults' => $request->get('num_of_adults'),
            'num_of_kids' => $request->get('num_of_kids'),
            'additional_request' => $request->get('additional_request'),
            'rooms' => $request->get('rooms'),
            'services' => $request->get('services'),
            'credit_card_id' => $request->get('credit_card_id'),
        ];

        try {
            $this->service->make(
                $start,
                $end,
                $admin_data,
                $customer_data
            );

            return response()->json();
        } catch (\Exception $e) {
            throw $e;
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a reservation.
     *
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = Reservation::find($id);

        if (is_null($reservation)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($reservation) {
                $reservation->delete();
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Performs checkin on a reservation.
     *
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function checkin(Request $request, $id)
    {
        $validation = Validator::make(
            $request->all(),
            ['deposit' => 'required']
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $reservation = Reservation::find($id);
        $deposit = $request->get('deposit');

        try {
            $this->service->checkin($reservation, $deposit);

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Performs checkout on a reservation.
     *
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\Response
     */
    public function checkout(Request $request, $id)
    {
        $reservation = Reservation::with('paymentLogs')->find($id);
        $payment_type_id = $request->get('payment_type_id');

        try {
            $this->service->checkout($reservation, $payment_type_id);

            return response()->json();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
