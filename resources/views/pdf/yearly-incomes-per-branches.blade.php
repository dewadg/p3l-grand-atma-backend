@extends('pdf.master')
@section('style')
  .year {
    display: block;
    margin-bottom: 15px;
  }

  .report {
    width: 100%;
    border-collapse: collapse;
  }

  .report thead tr th,
  .report tbody tr td,
  .report tfoot tr td {
    padding: 5px;
  }

  .report tfoot tr td {
    font-weight: bold;
  }
@endsection
@section('content')
  <h2 class="text-center">LAPORAN PENDAPATAN TAHUNAN</h2>
  <table class="report" border="1">
    <thead>
      <tr>
        <th class="text-center" width="50">No.</th>
        <th class="text-center">Tahun</th>
        <th class="text-center">Yogyakarta</th>
        <th class="text-center">Bandung</th>
        <th class="text-center">Total</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $row)
        <tr>
          <td class="text-center">{{ ($loop->index + 1) }}</td>
          <td class="text-center">{{ $row['year'] }}</td>
          <td class="text-right">Rp {{ number_format($row['jog'], 0, ',', '.') }}</td>
          <td class="text-right">Rp {{ number_format($row['bdg'], 0, ',', '.') }}</td>
          <td class="text-right">Rp {{ number_format($row['total'], 0, ',', '.') }}</td>
        </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr>
        <td class="text-right" colspan="4">TOTAL</td>
        <td class="text-right">Rp {{ number_format($total, 0, ',', '.') }}</td>
      </tr>
    </tfoot>
  </table>
  <p class="text-right">Dicetak pada {{ date('j F Y H:i') }}</p>
@endsection
