<?php

use Illuminate\Database\Seeder;
use App\PaymentType;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    private $data = [
        ['id' => 1, 'name' => 'Credit Card'],
        ['id' => 2, 'name' => 'Transfer Bank'],
        ['id' => 3, 'name' => 'Tunai'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $payment_type) {
            PaymentType::create($payment_type);
        }
    }
}
