<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationRoomMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_room_map', function (Blueprint $table) {
            $table->string('reservation_id');
            $table->foreign('reservation_id')
                ->references('id')->on('reservations');
            $table->string('payment_log_id');
            $table->foreign('payment_log_id')
                ->references('id')->on('payment_logs');
            $table->string('room_id', 9);
            $table->foreign('room_id')
                ->references('id')->on('rooms');
            $table->date('start');
            $table->date('end');
            $table->decimal('price', 15, 2);
            $table->decimal('subtotal', 15, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_room_map');
    }
}
