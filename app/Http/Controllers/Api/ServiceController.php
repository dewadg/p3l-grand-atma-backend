<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Returns available services.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Service::get();
    }

    /**
     * Stores new service.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'price' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                Service::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Returns single service.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $service = Service::find($id);

        if (is_null($service)) {
            return response()->json(null, 404);
        }

        return $service;
    }
    
    /**
     * Updates a service
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);

        if (is_null($service)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'price' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('id');

            DB::transaction(function () use ($service, $user_data) {
                $service->update($user_data);
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Deletes a service
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);

        if (is_null($service)) {
            return response()->json(null, 404);
        }

        try {
            DB::transaction(function () use ($service) {
                $service->delete();
            });

            return response()->json(null);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
