<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Reservation extends Model
{
    use SoftDeletes;
    
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'employee_id',
        'customer_id',
        'branch_id',
        'reservation_status_id',
        'type',
        'start',
        'end',
        'duration',
        'num_of_adults',
        'num_of_kids',
        'dp',
        'deposit',
        'total',
        'additional_request',
        'checkin_at',
        'checkout_at',
    ];

    /**
     * Casted attributes.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'dp' => 'double',
        'deposit' => 'double',
        'total' => 'double',
        'smoking_rooms' => 'boolean',
    ];

    /**
     * Sets the primary key to non incrementing.
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
        'deleted_at',
        'checkin_at',
        'checkout_at',
    ];

    /**
     * Relationship to Employee.
     *
     * @return Employee
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * Relationship to Customer.
     *
     * @return void
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Relationship to Branch.
     *
     * @return Branch
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    /**
     * Relationship to ReservationStatus.
     *
     * @return ReservationStatus
     */
    public function status()
    {
        return $this->belongsTo(ReservationStatus::class, 'reservation_status_id');
    }

    /**
     * Relationship to Room
     *
     * @return array
     */
    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'reservation_room_map')
            ->using(ReservationRoom::class);
    }

    /**
     * Relationship to Service
     *
     * @return array
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'reservation_service_map')
            ->using(ReservationService::class);
    }

    /**
     * Relationship to PaymentLog.
     *
     * @return array
     */
    public function paymentLogs()
    {
        return $this->hasMany(PaymentLog::class);
    }

    /**
     * Generates unique ID.
     *
     * @return string
     */
    public static function generateId($is_group = false)
    {
        $type = $is_group ? 'G' : 'P';
        $index = self::withTrashed()->get()->count() + 1;

        return $type
            . date('d')
            . date('m')
            . date('y')
            . str_pad((string) $index, 3, '0', STR_PAD_LEFT);
    }

    /**
     * Does a checkin on the current model.
     *
     * @return void
     */
    public function doCheckin()
    {
        $this->update(['checkin_at' => Carbon::now()]);
    }

    /**
     * Does a checkout on the current model.
     *
     * @return void
     */
    public function doCheckout()
    {
        $this->update(['checkout_at' => Carbon::now()]);
    }

    /**
     * Removes all reservations permanently!
     *
     * @return void
     */
    public static function resetAll()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::statement('TRUNCATE TABLE payment_logs');
        DB::statement('TRUNCATE TABLE reservation_service_map');
        DB::statement('TRUNCATE TABLE reservation_room_map');
        DB::statement('TRUNCATE TABLE reservations');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');        
    }
}
