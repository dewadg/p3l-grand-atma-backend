<?php

use Illuminate\Database\Seeder;
use App\RoomType;

class RoomTypesTableSeeder extends Seeder
{
    private $data = [
        ['id' => 'SP', 'name' => 'Kamar Superior' , 'description' => 'Kamar Superior', 'size' => '22 meter persegi ', 'price' => 320000,  'capacity' =>  2],
        ['id' => 'DD', 'name' => 'Kamar Double Deluxe' , 'description' => 'Kamar Double Deluxe', 'size' => '24 meter persegi ', 'price' => 380000,  'capacity' =>  2],
        ['id' => 'ED', 'name' => 'Kamar Executive Deluxe' , 'description' => 'Kamar Executive Deluxe', 'size' => '36 meter persegi ', 'price' => 400000,  'capacity' =>  2],
        ['id' => 'JS', 'name' => 'Kamar Junior Suite' , 'description' => 'Kamar Junior Suite', 'size' => '46 meter persegi ', 'price' => 420000,  'capacity' =>  2],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $room_type) {
            RoomType::create($room_type);
        }
    }
}
