<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypeSeasonMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_type_season_map', function (Blueprint $table) {
            $table->string('room_type_id', 2);
            $table->foreign('room_type_id')
                ->references('id')->on('room_types');
            $table->unsignedInteger('season_id');
            $table->foreign('season_id')
                ->references('id')->on('seasons');
            $table->decimal('price', 15, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_type_season_map');
    }
}
