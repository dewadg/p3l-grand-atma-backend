<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>
    <style>
      body {
        font-family: "Arial", sans-serif;
      }

      .text-center {
        text-align: center;
      }

      .text-right {
        text-align: right;
      }

      header {
        padding-bottom: 15px;
        margin-bottom: 15px;
        border-bottom: 1px solid #ccc;
      }

      .header-image {
        width: 460px;
      }

      header p {
        font-size: 18px;
        margin: 0;
      }

      @yield('style')
    </style>
  </head>
  <body>
    <header>
      <div align="center">
        <img class="header-image" src="{{ public_path() . '/images/site-logo.png' }}">
        <p align="center">Jl. P. Mangkubumi No. 18 Yogyakarta 55233<br>Telp. (0274) 487711</p>
      </div>
    </header>
    @yield('content')
  </body>
</html>
