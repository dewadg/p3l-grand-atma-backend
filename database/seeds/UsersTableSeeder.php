<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    private $data = [
        [
            'id' => 1,
            'email' => 'adhyaksaj@gmail.com',
            'name' => 'Adhyaksa Jaya',
            'password' => 'degeGanteng',
            'role_id' => 1,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $user) {
            $user['password'] = bcrypt($user['password']);

            User::create($user);
        }
    }
}
