<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'file_name',
        'path',
    ];

    /**
     * Returns image URL.
     *
     * @return string
     */
    public function url()
    {
        return env('APP_URL') . '/storage/' . $this->path;
    }
}
