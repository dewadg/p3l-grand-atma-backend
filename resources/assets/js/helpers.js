import numeral from 'numeral';
import moment from 'moment';

function asCurrency(number) {
  const currency = numeral(number)
    .format('0,0');

  return `Rp ${currency}`;
}

function asDate(date) {
  if (!date) {
    return '';
  }

  return moment(date).format('D/M/YYYY');
}

export default {
  asCurrency,
  asDate,
};
