<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ReportService;
use PDF;

class ReportController extends Controller
{
    /**
     * Stores the report service instance.
     *
     * @var ReportService
     */
    protected $service;

    public function __construct()
    {
        $this->service = new ReportService;
    }

    /**
     * Returns registered customers on the year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function customers($year)
    {
        $data = $this->service->customers($year);
        $pdf = PDF::loadView('pdf.customers', $data);

        return $pdf->stream();
    }

    /**
     * Returns count of customers on available room types.
     *
     * @param string $year
     * @param string $month
     * @return Illuminate\Http\Response
     */
    public function roomTypes($year, $month)
    {
        $data = $this->service->roomTypes($year, $month);
        $pdf = PDF::loadView('pdf.room-types', $data);

        return $pdf->stream();
    }

    /**
     * Returns top customers by transaction.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function topCustomers($year)
    {
        $data = $this->service->topCustomers($year);
        $pdf = PDF::loadView('pdf.top-customers', $data);

        return $pdf->stream();
    }

    /**
     * Returns each months incomes by year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function monthlyIncomes($year)
    {
        $data = $this->service->monthlyIncomes($year);
        $pdf = PDF::loadView('pdf.monthly-incomes', $data);

        return $pdf->stream();
    }

    /**
     * Returns each branches' incomes by year.
     *
     * @param string $year
     * @return Illuminate\Http\Response
     */
    public function yearlyIncomesPerBranches($year)
    {
        $data = $this->service->yearlyIncomesPerBranches($year);
        $pdf = PDF::loadView('pdf.yearly-incomes-per-branches', $data);

        return $pdf->stream();
    }
}
