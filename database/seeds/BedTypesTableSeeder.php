<?php

use Illuminate\Database\Seeder;
use App\BedType;

class BedTypesTableSeeder extends Seeder
{
    /**
     * Initial data.
     *
     * @var array
     */
    private $data = [
        [
            'name' => '1 Double',
            'capacity' => 2,
        ],
        [
            'name' => '1 Twin',
            'capacity' => 1,
        ],
        [
            'name' => '2 Twin',
            'capacity' => 2,
        ],
        [
            'name' => '1 King',
            'capacity' => 2,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $bed_type) {
            BedType::create($bed_type);
        }
    }
}
